class QMaster{

 // **** Node structure  ********
 // 1. nodeType    - Info, ...
 // 2. id
 // 3. Question
 // 4. parentId
 // 5. next/edges
 // 6. inputType    - 'text, choice'
 // 7. inputOptions - Array [ {value, text }]
 // 8. validator
 // 9. message  if nodeType is 'Info'
 //10. callback
 // *****************************

 // ******* Config *************
 // 1. targetEl
 // 2. startNode
 // 3. nodes
 // 4. onFinish
 // 5. persistData
 // ****************************

  constructor(config){
    if(!window.jQuery || !config.targetEl || !config.nodes || !(config.onFinish instanceof Function) ){
      throw 'Data not enough to instantiate'
    }
    this.config     = config
    this.nodes      = config.nodes
    this.flowStack  = []
    this.savedData  = {}
    this.init()

    $(document).keypress(function(e) {
    if(e.which == 13) {
        $(function(){$('#Next').click()})
    }
});
  }

  init(){
    this.flowStack  = []
    $(this.config.targetEl).load(this.config.template || 'http://localhost:8000/src/temp.html', ()=>{
      this.curNode = this.config.startNode
      this.loadNode()
      $('#Next').on('click.qMaster',()=>{
        this.onNext()
      })
      $('#Back').on('click.qMaster',()=>{
        this.onBack()
      })
    })
  }

  validate(curAnswer, nodeId){
    let node = this.nodes[nodeId || this.curNode]
    if(node.validator)
      return node.validator.test(curAnswer)
    return true
  }

  onNext(){
    let node      = this.nodes[this.curNode]
    let curAnswer

    if(node.nodeType =='Question'){
      curAnswer = this.getAnswer()
      if(!this.validate(curAnswer)){
        console.log("Failed the regex" + node.validator)
        return
      }
      this.flowStack.push({nodeId:this.curNode,answer:curAnswer})
      this.curNode  = node.next || node.edges&&node.edges[curAnswer]

      this.addtoHistory(node.Question, curAnswer)
    }
    else if(node.nodeType == 'Info'){
      this.curNode  = node.next
    }

    if(node.callback instanceof Function){
      let reply = node.callback(this.genReport())
      $('#Info').html(reply.msg)
      if(reply.proceed == false){
        //revert Back
        this.flowStack.pop()
        this.curNode    = node.id
        this.removeLastHistory()
        return
      }
    }

  //if no next/edges property then it is the end of the flow
    if(this.curNode)
      this.loadNode(this.curNode)
    else { // All Questions caught up..
      this.config.onFinish(this.genReport())
      console.log('Restarting....')
      this.init()
    }
  }


  onBack(){
    if(this.flowStack.length == 0)
      return
    this.curNode = this.flowStack.pop().nodeId
    this.removeLastHistory()
    this.loadNode(this.curNode)
  }

  genReport(){
      var answer      = {}
      var cur_Obj     = answer
      var cur_parent  = null

      for(let i=0; i<this.flowStack.length;i++){
          let node        = this.nodes[this.flowStack[i].nodeId]
          let curAnswer   = this.flowStack[i].answer

          //Descending one level
          if(node.parentId!=cur_parent){
            cur_Obj[node.parentId] = {}
            cur_Obj                 = cur_Obj[node.parentId]
            cur_parent              = node.parentId
          }

          cur_Obj[node.id]          = curAnswer
      }

      return answer
  }

//Gets the user's answer return null if validation fails
  getAnswer(){
    let node = this.nodes[this.curNode]
    let answer;
    if(node.inputType=='choice')
      answer =  $('#InputOptions').val()
    else // if(node.inputType == 'text')
      answer =  $('#InputBox').val()

    if(this.config.persistData)
    this.savedData[this.curNode] = answer

    return answer
  }

  clearInputs(){
    $('#InputOptions').html('').hide()
    $('#InputBox').val('').hide()
    $('#Info').html('')
  }

  loadNode(nodeId){
    nodeId   = nodeId || this.curNode
    let node =  this.nodes[nodeId]
    if(!node)
      throw 'Node not found: ' + nodeId
    this.clearInputs()

    if(node.nodeType=='Info'){
      $('#Question').html(node.msg)
      return
    }
    else if(node.nodeType=='Question'){
      $('#Question').html(node.Question)
      if(node.inputType=='choice'){
        $('#InputOptions').show()
        node.inputOptions.forEach((option,idx)=> {
            $('#InputOptions').append($('<option>', option))
        })
        $('[value="'+ this.savedData[nodeId] +'"]').attr('selected',true) // does this look messier ??
      }
      else{
        $('#InputBox').show()
        $('#InputBox').val(this.savedData[nodeId])
      }
    }
  }

  addtoHistory(question,answer){
    $('.history').append('<div class="history-row"><div class="history-question">'+ question
        + '</div><div class="history-answer">' + answer + '</div></div>'  )
    $('.history').animate({ scrollTop: $('.history')[0].scrollHeight }, "fast")
  }
  removeLastHistory(){
    $('.history .history-row').last().remove()
  }

}
