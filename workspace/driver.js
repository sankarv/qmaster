const inputJSON = {
  'welcome': {
    nodeType    : 'Info',
    id          : 'welcome',
    msg         : 'Welcome to samsung upgrade programme',
    next        : 'mobileno'
  },
  'mobileno' : {
    nodeType    : 'Question',
    id          : 'mobileno',
    inputType   : 'text',
    validator   : /^[0-9]{5,5}$/,
    Question    : 'Carrier Mobile no',
    next        : 'carrier_pass'
  },
  'carrier_pass' : {
    nodeType    : 'Question',
    id          : 'carrier_pass',
    Question    : 'Your password',
    inputType   : 'text',
    next        : 'connection',
    callback    : (answer)=>{
      if(answer["carrier_pass"]=="sankar")
        return {proceed:true}
      return {proceed:false,msg:"passorwd is sankar"}
    }
  },
  'connection': {
    nodeType    : 'Question',
    id          : 'connection',
    inputType   : 'choice',
    Question    : 'Would you like to transfer your old number or get a new one?',
    inputOptions: [{value:"transfer",text:"transfer a old no"},{value:"new",text:"get a new no"}],
    edges       : {transfer:"finish","new" : "plans"},
    parentId    : "Connection"
  },
  'plans': {
    nodeType    : 'Question',
    id          : 'plans',
    parentId    : 'Connection',
    inputType   : 'choice',
    Question    : 'Choose a plan',
    inputOptions: [{value:'1',text:"1gb/month"},{value:"2",text:"512mb/0.5month"}],
    next        : 'wPersonal'
  },
  'wPersonal': {
    nodeType    : 'Info',
    msg         : 'Please provide the credit info',
    next        : 'name'
  },
  'name':{
    nodeType    : 'Question',
    id          : 'name',
    inputType   : 'text',
    Question    : 'Your Name',
    parentId    : 'Credit Info',
    next        : 'ssn'
  },
  'ssn':{
    nodeType    : 'Question',
    id          : 'ssn',
    inputType   : 'text',
    parentId    : 'Credit Info',
    Question    : 'Social security no',
    next        : 'address',
  },
  'address':{
    nodeType    : 'Question',
    id          : 'address',
    inputType   : 'text',
    Question    : 'Address',
    parentId    : 'Credit Info',
    next        : 'dob'
  },
  'dob':{
    nodeType    : 'Question',
    id          : 'dob',
    inputType   : 'text',
    validator   : /^[0-3][0-9]\/[01][0-9]\/[0-9]{4,4}$/,
    Question    : 'Date of Birth',
    parentId    : 'Credit Info',
    next        : 'finish'
  },
  'finish':{
    nodeType    : 'Info',
    msg         : 'End of flow'
  }
}


$(document).ready(function(){
  const config    = {
      targetEl:"#test",
      startNode: 'welcome',
      persistData: false,
      nodes:inputJSON,
      onFinish: function(report){
        console.log(report)
        }
    }
  const qMaster   = new QMaster(config)
  window.qMaster  = qMaster
})
